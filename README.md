# git的分支操作
## 创建本地分支
`git checkout -b <新的本地分支名称>`
## 创建远端分支
`git push origin <远端分支名称>`
## 切换本地分支
`git checkout <本地分支名称>`
## 删除远端分支
`git push --delete origin <远端分支名称>`
## 删除本地分支
`git branch -D <本地分支名称>`
